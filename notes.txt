// Node.js Routing w/ HTTP Methods
// Access URL endpoints using HTTP methods

/* 
   Learn how to manage server-side code using HTTP methods
   - I can determine the response needed by the client application
   Perform create & retrieve operations using JS objects
   - I know how to send requests to my server
   Use Postman to send requests to our Node.js server
   - I can replicate responses sent to my client application
*/
/* 
   
   This is sufficient for retrieving resources from different endpoints.

   HTTP Methods

   Node.js Routing w/ HTTP methods

   GET - Retrieves resources
   POST - Sends data for creating a resource
   PUT - Sends data for updating a resource
   DELETE - deletes specified resource

*/

// Postman Client
// Node.js

// Postman Client to improve the experience of sending requests, inspecting responses, and debugging.
