let http = require("http");
const { request } = require("https");
const { json } = require("stream/consumers");
let port = 4000;

// Mock Database
let directory = [
  {
    name: "Brandon",
    email: "brandon@gmail.com",
  },
  {
    name: "Jobert",
    email: "jobert@mail.com",
  },
];

const server = http.createServer((req, res) => {
  // When the '/users' route is accessed with a method of "GET" we will send the directory (mock database) list of users.

  if (req.url == "/users" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "application/json" });
    //   writeHead is the header
    //   write is the body
    res.write(JSON.stringify(directory));
    res.end();
  }

  // We want to receive the content of the request and save it to the mock database.
  // Content will be retrieve from the request body.
  // Parse the received JSON and parse it into JavaScript Object
  // add the parse object to the directory (mock database)
  if (req.url == "/users" && req.method == "POST") {
    res.writeHead(200, { "Content-Type": "application/json" });
    //   This will act as a placeholder for the resource/data to be created later on.
    let requestBody = "";
    // data step - this reads "data" stream and process it as a request body.
    //  at this point, requestBody has the
    req.on("data", (data) => {
      console.log(data);
      //  Assigns the data retrieved from the data to stream to requestBody
      //  at this point, "requestBody" has the
      requestBody += data;
      //  to show that the chunk of information stored in the requestBody.
      console.log(requestBody);
    });
    //  response end step - only runs after the request has completely sent.
    req.on("end", () => {
      console.log(typeof requestBody);
      requestBody = JSON.parse(requestBody);
      console.log(typeof requestBody);
      directory.push(requestBody);
      console.log(directory);
      res.writeHead(200, { "Content-Type": "application/json" });
      res.write(JSON.stringify(requestBody));
      res.end();
    });

    //   This will act as a place holder for the resource/data to be created later on.
    res.write(JSON.stringify(directory));
    res.end();
  }
});

server.listen(port);
console.log(`Server running at localhost: ${port}`);
